package pl.patrykpacewicz.quantum;

import org.jscience.mathematics.number.Complex;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class GatesTest {
    @Test
    public void shouldUseNotGate() throws Exception {
        Qubit testedQubit = new Qubit(Complex.ONE, Complex.ZERO);
        Qubit expectedQubit = new Qubit(Complex.ZERO, Complex.ONE);

        Gates gates = new Gates();

        assertThat(gates.not(testedQubit)).isEqualTo(expectedQubit);
    }

    @Test
    public void shouldUseHadamardGateOnZeroQubit() throws Exception {
        double hadamardElement = 1 / Math.sqrt(2);

        Qubit testedQubit = Qubit.ZERO;

        Qubit expectedQubit = new Qubit(
                Complex.ONE.times(hadamardElement),
                Complex.ONE.times(hadamardElement)
        );

        Gates gates = new Gates();

        assertThat(gates.hadamard(testedQubit)).isEqualTo(expectedQubit);
    }

    @Test
    public void shouldUseHadamardGateOnOneQubit() throws Exception {
        double hadamardElement = 1 / Math.sqrt(2);

        Qubit testedQubit = Qubit.ONE;

        Qubit expectedQubit = new Qubit(
                Complex.ONE.times(hadamardElement),
                Complex.ONE.times(hadamardElement).opposite()
        );

        Gates gates = new Gates();

        assertThat(gates.hadamard(testedQubit)).isEqualTo(expectedQubit);
    }
}
