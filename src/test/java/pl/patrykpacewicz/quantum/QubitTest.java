package pl.patrykpacewicz.quantum;

import org.jscience.mathematics.number.Complex;
import org.jscience.mathematics.vector.ComplexMatrix;
import org.junit.Test;
import pl.patrykpacewicz.quantum.exception.InvalidQubitRepresentationException;

import static org.fest.assertions.Assertions.assertThat;

public class QubitTest {
    @Test(expected = InvalidQubitRepresentationException.class)
    public void shouldThrowExceptionWhenQubitIsNotValid() {
        new Qubit(Complex.ONE, Complex.ONE);
    }

    @Test
    public void shouldReturnQubitEqualZero() {
        Qubit zeroQubit = Qubit.ZERO;

        assertThat(zeroQubit.getZeroRepresentation().getReal()).isEqualTo(1);
        assertThat(zeroQubit.getZeroRepresentation().getImaginary()).isEqualTo(0);
        assertThat(zeroQubit.getOneRepresentation().getReal()).isEqualTo(0);
        assertThat(zeroQubit.getOneRepresentation().getImaginary()).isEqualTo(0);
    }

    @Test
    public void shouldReturnQubitEqualOne() {
        Qubit oneQubit = Qubit.ONE;

        assertThat(oneQubit.getZeroRepresentation().getReal()).isEqualTo(0);
        assertThat(oneQubit.getZeroRepresentation().getImaginary()).isEqualTo(0);
        assertThat(oneQubit.getOneRepresentation().getReal()).isEqualTo(1);
        assertThat(oneQubit.getOneRepresentation().getImaginary()).isEqualTo(0);
    }

    @Test
    public void shouldReturnMatrixRepresentationOfQubitOne() {
        ComplexMatrix oneMatrix = Qubit.ONE.getAsMatrix();

        assertThat(oneMatrix.getNumberOfColumns()).isEqualTo(1);
        assertThat(oneMatrix.getNumberOfRows()).isEqualTo(2);
        assertThat(oneMatrix.get(0, 0)).isEqualTo(Complex.ZERO);
        assertThat(oneMatrix.get(1, 0)).isEqualTo(Complex.ONE);
    }

    @Test
    public void shouldReturnMatrixRepresentationOfQubitZero() {
        ComplexMatrix oneMatrix = Qubit.ZERO.getAsMatrix();

        assertThat(oneMatrix.getNumberOfColumns()).isEqualTo(1);
        assertThat(oneMatrix.getNumberOfRows()).isEqualTo(2);
        assertThat(oneMatrix.get(0, 0)).isEqualTo(Complex.ONE);
        assertThat(oneMatrix.get(1, 0)).isEqualTo(Complex.ZERO);
    }

    @Test
    public void shouldRepresentAsString() {
        assertThat(Qubit.ZERO.toString()).isEqualTo("(1.0 + 0.0i)0> + (0.0 + 0.0i)1>");
        assertThat(Qubit.ONE.toString()).isEqualTo("(0.0 + 0.0i)0> + (1.0 + 0.0i)1>");
    }

    @Test
    public void shouldEqualTwoQubits() {
        assertThat(Qubit.ZERO).isEqualTo(new Qubit(Complex.ONE, Complex.ZERO));
        assertThat(Qubit.ONE).isEqualTo(new Qubit(Complex.ZERO, Complex.ONE));
    }

    @Test
    public void shouldReturnValue() {
        assertThat(Qubit.ZERO.getValue()).isFalse();
        assertThat(Qubit.ONE.getValue()).isTrue();
    }

}
