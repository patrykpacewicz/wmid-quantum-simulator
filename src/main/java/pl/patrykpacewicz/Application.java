package pl.patrykpacewicz;

import com.google.inject.Guice;
import org.apache.log4j.Logger;
import pl.patrykpacewicz.guice.ApplicationModule;

public class Application {
    private Logger logger = Logger.getLogger(getClass());

    public static void main(String[] args) {
        Guice.createInjector(new ApplicationModule()).getInstance(Application.class).run();
    }

    public void run() {
        logger.info("Hello World!");
    }
}
