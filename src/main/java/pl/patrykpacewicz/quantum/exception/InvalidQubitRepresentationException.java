package pl.patrykpacewicz.quantum.exception;

import org.jscience.mathematics.number.Complex;

public class InvalidQubitRepresentationException extends RuntimeException {
    private final Complex zeroRepresentation;
    private final Complex oneRepresentation;

    public InvalidQubitRepresentationException(Complex zeroRepresentation, Complex oneRepresentation) {
        super(String.format("Invalid zero: %s, and one: %s representations", zeroRepresentation, oneRepresentation));
        this.zeroRepresentation = zeroRepresentation;
        this.oneRepresentation = oneRepresentation;
    }

    public Complex getZeroRepresentation() {
        return zeroRepresentation;
    }

    public Complex getOneRepresentation() {
        return oneRepresentation;
    }
}
