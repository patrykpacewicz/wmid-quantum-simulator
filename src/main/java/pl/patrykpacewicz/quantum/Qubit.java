package pl.patrykpacewicz.quantum;

import org.jscience.mathematics.number.Complex;
import org.jscience.mathematics.vector.ComplexMatrix;
import org.jscience.mathematics.vector.ComplexVector;
import pl.patrykpacewicz.quantum.exception.InvalidQubitRepresentationException;

import java.util.Objects;
import java.util.Random;

public class Qubit {
    public static final Qubit ZERO = new Qubit(Complex.ONE, Complex.ZERO);
    public static final Qubit ONE = new Qubit(Complex.ZERO, Complex.ONE);

    private Complex zeroRepresentation;
    private Complex oneRepresentation;

    public Qubit(Complex zeroRepresentation, Complex oneRepresentation) {
        this.zeroRepresentation = zeroRepresentation;
        this.oneRepresentation = oneRepresentation;

        if (!validateRepresentation()) {
            throw new InvalidQubitRepresentationException(zeroRepresentation, oneRepresentation);
        }
    }

    public Complex getZeroRepresentation() {
        return zeroRepresentation;
    }

    public Complex getOneRepresentation() {
        return oneRepresentation;
    }

    private boolean validateRepresentation() {
        double zeroPow = Math.pow(zeroRepresentation.magnitude(), 2);
        double onePow = Math.pow(oneRepresentation.magnitude(), 2);
        return (round(zeroPow) + round(onePow)) == 1;
    }

    private double round(double i) {
        return Math.round(i*100000000.0)/100000000.0;
    }

    public ComplexMatrix getAsMatrix() {
        return ComplexMatrix.valueOf(
                ComplexVector.valueOf(zeroRepresentation),
                ComplexVector.valueOf(oneRepresentation)
        );
    }

    public boolean getValue() {
        double zeroProbability = Math.pow(zeroRepresentation.magnitude(), 2);
        return new Random().nextDouble() > zeroProbability;
    }

    @Override
    public String toString() {
        return String.format("(%s)0> + (%s)1>", zeroRepresentation, oneRepresentation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(zeroRepresentation, oneRepresentation);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Qubit other = (Qubit) obj;
        return Objects.equals(this.zeroRepresentation, other.zeroRepresentation) && Objects.equals(this.oneRepresentation, other.oneRepresentation);
    }
}
