package pl.patrykpacewicz.quantum;

import org.jscience.mathematics.number.Complex;
import org.jscience.mathematics.vector.ComplexMatrix;
import org.jscience.mathematics.vector.ComplexVector;

public class Gates {
    public Qubit not(Qubit qubit) {
        ComplexMatrix gate = ComplexMatrix.valueOf(
                ComplexVector.valueOf(Complex.ZERO, Complex.ONE),
                ComplexVector.valueOf(Complex.ONE, Complex.ZERO)
        );

        return passThroughGate(qubit, gate);
    }

    public Qubit hadamard(Qubit qubit) {

        Complex hadamardElement = Complex.ONE.times(1 / Math.sqrt(2));

        ComplexMatrix gate = ComplexMatrix.valueOf(
                ComplexVector.valueOf(hadamardElement, hadamardElement),
                ComplexVector.valueOf(hadamardElement, hadamardElement.opposite())
        );

        return passThroughGate(qubit, gate);
    }

    private Qubit passThroughGate(Qubit qubit, ComplexMatrix gate) {
        ComplexMatrix notMatrix = gate.times(qubit.getAsMatrix());
        return new Qubit(notMatrix.getColumn(0).get(0), notMatrix.getColumn(0).get(1));
    }
}
